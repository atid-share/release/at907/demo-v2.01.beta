## RELEASE HISTORY

##### _Release date: 2021-12-14_

#### Demo version : {+V2.01.2021121400.beta+}

#### Suported FW version:  v4.9.6.1 and above


## What's New

 - [ ] Rail Tag Inventory Feature added
 <details>
     <summary markdown="span" >Screenshots</summary>
      <div align="left">
        <img width="22%" src="https://gitlab.com/atid-share/asset/-/raw/744f96b32462472e41c1b2bde4ae0ccfccc2fa50/sample_00.png"  alt="Setting screen" title="Settings"</img>
        <img width="22%" src="https://gitlab.com/atid-share/asset/-/raw/744f96b32462472e41c1b2bde4ae0ccfccc2fa50/sample_01.png" alt="List screen" title="Setting Lists"></img>
        <img width="22%" src="https://gitlab.com/atid-share/asset/-/raw/744f96b32462472e41c1b2bde4ae0ccfccc2fa50/sample_02.png" alt="dialog " title="Option dialog"></img>
       </div>
      </details>


## Support
    	
inquiry@atid1.com

